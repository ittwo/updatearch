#!/bin/python

import subprocess
import sys
from tkinter import *

# create window window

window = Tk()
window.title(' Archlinux Update ')
window.geometry('200x105')

window.Label = "Run Updates Now?"
window.resizable(False, False)

updater = "/home/ralvez/bin/run.d/updates"
terminal = "alacritty"


def runUpdate():
    subprocess.Popen([terminal, "-e", updater])
    window.destroy()


frLegend = Frame(window)
frButtons = Frame(window)

label = Label(frLegend,
              text="\n\nUpdate system now?\n",
              justify=LEFT)
label.pack(side=LEFT)

run_update = Button(frButtons,
                    text="Update",
                    justify=RIGHT,
                    command=runUpdate)
run_update.pack(side=RIGHT)

cancel = Button(frButtons,
                text="Cancel",
                command=window.destroy)
cancel.pack()

frLegend.pack(padx=1, pady=1)
frButtons.pack(padx=10, pady=10)

# Tkinter event loop
window.mainloop()
