#!/usr/bin/env python

""" + --------------------------------------------------------------- +
    checkupdates: returns a lit of packages to be updated.

    This program will use "checkupdates" from pacman-contrib script 
    to get the list of updates, count them, and produce a report.

    DEPENDENCIES: pacman-contrib script. (pacman -S pacman-contrib)
    + --------------------------------------------------------------- + """

import os
import sys
import subprocess


def checkupdates():

    updates = subprocess.call(
        ['checkupdates'], stdout=open('/tmp/updates.txt', 'w'))

    if updates == 0:
        number_of_updates = len(open('/tmp/updates.txt').readlines())
        if (number_of_updates > 0):
            return number_of_updates
        else:
            return 0
