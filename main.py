#!/usr/bin/env python

from utils import updates


def main():
    global count
    count = updates.checkupdates()

    if count == None:
        count = 0
        print(f"{count} updates")
    elif (count > 1):
        print(f" {count} updates")
    else:
        print(f"{count} update")


if __name__ == '__main__':
    main()
